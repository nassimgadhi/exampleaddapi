import unittest
import requests
import json

from codeAPI import api

IP = "127.0.0.1"
#IP = "18.212.89.93"
PORT = "5555"


class DeploymentTest(unittest.TestCase):

	def test_hello(self):
		response = requests.get("http://" + IP + ":" + PORT + "/")
		self.assertEqual(200, response.status_code)
		self.assertIn('Message quelconque', response.content.decode("utf-8"))

	def test_sum(self):
		response = requests.get("http://" + IP + ":" + PORT + "/sum?a=2&b=3")
		self.assertEqual(200, response.status_code)
		map = json.loads(response.content.decode("utf-8"))
		self.assertEqual(5, map['rep'])

	def test_sum_missingParam(self):
		response = requests.get("http://" + IP + ":" + PORT + "/sum?a=2")
		self.assertEqual(400, response.status_code)
